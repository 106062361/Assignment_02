# Software Studio 2019 Spring Assignment 2

# Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

# Website Detail 
## 1. Game Process ~15%
- Start Menu
![](https://i.imgur.com/yvvuZRo.png)
- Game View
![](https://i.imgur.com/4eU0i7l.png)
- Game Over
![](https://i.imgur.com/fsPbdYL.png)

## 2. Basic Rules ~20%
- **Player** (astronout): TPress left/right key to move, press SPACE to shoot upwards.
- Player have 5 life. It's life decreases when hit by enemy's bullet. Player dies when  he is hit 5 times.
- **Enemy** (UFO): Each enemy will either comes from the left or the right side of the game window. Enemy generates bullets and dies when hit by player's bullet. 
- Bullets have no effect on bullets.
- When the player hit an enemy, the score increase by 1.
- **Map**: Background move down throughout the game.

## 3. UI ~5%
- The following information is shown in the **top left** of the game view
    - Score
    - Volume: from -1 to 1, Precise to 0.1
- The following information is shown in the **top right** of the game view
They will be reset in each level.
    - Life (Number of life left, aka player health)
    - Bomb (Number of bomb left)
    - Invisible (Number of invisible skill can be trigger)
- **Game Pause**: Press P key to pause the game, press again to resume
![](https://i.imgur.com/enRFZWR.png)


# Basic Components Description : 
## 1. Jucify mechanisms ~15%
### Invisible Skill: 
- Player has a special skill which he can turn invisible for a few seconds.
- Trigger: Press V to trigger 
- Function: During this period, the player is untouchable by enemy's bullet. (Life will not decrease when attacked)
- Appearance: The player become gray when the skill is triggered

![](https://i.imgur.com/yvZNSBV.png)

### Levels
- After attacked every 10 enemies, the player will level up. 
- There is no maximum level limit.
- The followings will be reset in each level:
    - Player's life
    - No of bombs (special bullets)
    - Time of skills can be used
- The following properties vary in each level:
    - The time interval of enemy generation
    - The attack speed of enemies
    - The bullet speed od enemies
    - The moving speed of enemies
    - The attack speed of the player
    - The bullet speed of the player 
    - The moving speed of the player
    - The moving speed of the helper
    - The dropping speed of the potion
    - The moving speed of the background
- The following properties change every few levels
    - The maximum no of enemy 
    - The number of bombs (special bullets) the player can use in each level
    - How many times the invisible skill can be triggered in each level
## 2. Animations ~10%
### Player
- The recoil of the pistol when the player shoot
- The player shines in yellow when hitted
- When the player is holding bomb, he shakes his bomb in order to scare enemies
### Bullet
- The bullets gradually grow/expand when moving

## 3. Particle Systems ~10%
- The player bullets comes with star particles upon hitting enemy
- I don't want to add particles on enemy bullets to avoid distraction
- Particles appear on the bomb explodes
- Particles appear on level up

## 4. Sound effects ~5%
- Background music in start menu and game over: [River Flows in You by Yiruma](https://www.youtube.com/watch?v=VxoY-WE2I1M)
- Background music in game view: [Relaxing Disney Music](https://www.youtube.com/watch?v=N7mdFBc4XNs)
- Sound effect on player shooting: gun fires sound
- Sound effect on bullet hit: bubble pop sound
- Sound effect on bomb explosion: fireworks sound
- **Volume Control**: Press Up/Down Key to adjust the volume

## 5. Leaderboard 
Undefined

# Bonus Functions Description : 
## 1. Special bullet: Bomb
- Trigger: Hold B key to change the weapon, press SPACE key to fire
- Function: Clear all the enemies in the space by once, kill enemy boss.
- Appearance: The animation of player changes when holding B key
- Fancy effect on the bomb explosion.
- When the player level up, the number of bombs he can use increases

## 2. Boss
- The boss is 2x bigger than other enemy, with slower speed
- The boss can fires 3 bullet each times in different direction
- The boss has longer generating interval than the other enemies
- Its attack speed is also higher than other enemy
- The generating interval, attack speed, moving speed, bullet speed of enemy boss vary by levels
- The enemy boss can only destroyed by bomb, small bullets have no effect on it.
- If a boss is killed, the score increase by 3
![](https://i.imgur.com/PTxUDdk.png)


## 3. Helper (satellite) and Potion
- The helper will come from left or right, with slow moving speed.
- It will drop a potion randomly, which can recharge a life.
- The generating interval, moving speed, potion speed of helper vary by levels
- The helper can be killed by bullets. 
- If the helper is killed, the score will decrease by 3, and the player will not get any potion.  

![](https://i.imgur.com/kQCx1n6.png)
