var loadState = {
    preload: function() {
        game.load.image('background', 'assets/background.png');
        game.load.image('enemy', 'assets/ufo.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('helper', 'assets/satellite.png');
        game.load.image('potion', 'assets/potion.png');
        game.load.image('bomb', 'assets/bomb.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('particle', 'assets/star.png');
        game.load.spritesheet('player', 'assets/player.png', 140, 180);
        game.load.audio('bgm1', ['assets/river-flows-in-you.mp3', 'assets/river-flows-in-you.ogg']);
        game.load.audio('bgm2', ['assets/disney.mp3', 'assets/disney.ogg']);
        game.load.audio('effect1', ['assets/gunshot.mp3', 'assets/gunshot.ogg']);
        game.load.audio('effect2', ['assets/fireworks.mp3', 'assets/fireworks.ogg']);
        game.load.audio('effect3', ['assets/bubble.mp3', 'assets/bubble.ogg']);
    },
    create: function() {
        game.add.backgroundColor = '#000000';
        game.state.start('menu');
    }
};

var menuState = {
    create: function() {
        game.add.backgroundColor = '#000000';
        game.add.image(0, 0, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        this.enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        game.add.text(game.width/2, game.height/4, 'Star War', {font: '60px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 500, 'Press Enter to Start', {font: '20px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);

        if(!gameOverState.bgm || !gameOverState.bgm.isPlaying) {
            this.bgm = game.add.audio('bgm1', game.global.volume, true);
            this.bgm.play();
        }
    },
    update: function() {
        if(this.enterKey.isDown) {
            if(this.bgm) this.bgm.stop();
            game.state.start('game');
        }
        this.controlVolume();
    },
    controlVolume: function() {
        if(this.cursor.up.isDown && game.global.volume<1) {
            game.global.volume += 0.1;
        }
        if(this.cursor.down.isDown && game.global.volume>0) {
            game.global.volume -= 0.1;
        }
        this.bgm.volume = game.global.volume;
    }
};

var gameState = {
    create: function() {
        game.stage.backgroundColor = '#000000';
        this.bgtile = game.add.tileSprite(0, 0, game.cache.getImage('background').width, 600, 'background');

        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.weaponKey = game.input.keyboard.addKey(Phaser.Keyboard.B);
        this.skillKey = game.input.keyboard.addKey(Phaser.Keyboard.V);
        game.input.keyboard.addKey(Phaser.Keyboard.P).onDown.add(this.controlPause, this);
        
        this.player = game.add.sprite(game.width/2, game.height-18*7, 'player');
        this.player.scale.setTo(0.7, 0.7);
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.player.animations.add('fireGun', [1, 0], 6, false);
        this.bombAnimation = this.player.animations.add('holdingBomb', [3, 4], 8, true);
        this.player.animations.add('injured', [2, 0, 2, 0, 2, 0], 15, false);

        this.bulletGroup = game.add.group();
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup.createMultiple(30, 'bullet');
        this.bulletGroup.setAll('anchor.x', 0.5);
        this.bulletGroup.setAll('anchor.y', 0.5);
        this.bulletGroup.setAll('outOfBoundsKill', true);
        this.bulletGroup.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.bulletGroup);

        this.enemyGroup = game.add.group();
        this.enemyGroup.enableBody = true;
        this.enemyGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyGroup.createMultiple(3 + game.global.level/4, 'enemy');
        this.enemyGroup.setAll('scale.x', 0.5);
        this.enemyGroup.setAll('scale.y', 0.5);
        this.enemyGroup.setAll('anchor.x', 0.5);
        this.enemyGroup.setAll('anchor.y', 1);
        this.enemyGroup.setAll('outOfBoundsKill', true);
        this.enemyGroup.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemyGroup);

        this.enemyBulletGroup = game.add.group();
        this.enemyBulletGroup.enableBody = true;
        this.enemyBulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBulletGroup.createMultiple(3, 'bullet');
        this.enemyBulletGroup.setAll('anchor.x', 0.5);
        this.enemyBulletGroup.setAll('anchor.y', 0.5);
        this.enemyBulletGroup.setAll('outOfBoundsKill', true);
        this.enemyBulletGroup.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemyBulletGroup);
        
        this.bulletEmitter = game.add.emitter(0, 0, 10);
        this.bulletEmitter.makeParticles('particle');
        this.bulletEmitter.setYSpeed(-150, 150);
        this.bulletEmitter.setXSpeed(-150, 150);
        this.bulletEmitter.setScale(0.1, 0, 0.1, 0, 800);
        this.bulletEmitter.gravity = 100;

        this.bombEmitter = game.add.emitter(game.width/2, game.height/2, 15);
        this.bombEmitter.makeParticles('particle');
        this.bombEmitter.setYSpeed(-150, 150);
        this.bombEmitter.setXSpeed(-150, 150);
        this.bombEmitter.setScale(0.5, 0, 0.5, 0, 75);
        this.bombEmitter.gravity = 100;

        if(!this.bgm || !this.bgm.isPlaying) {
            this.bgm = game.add.audio('bgm2', game.global.volume, true);
            this.bgm.play();
        }

        this.bulletTime = 0;
        game.time.events.loop(2500-40*game.global.level, this.addEnemy, this);
        game.time.events.loop(1500-20*game.global.level, this.createEnemyBullet, this);
        game.time.events.loop(11000-500*game.global.level, this.addHelper, this);
        game.time.events.loop(23000-800*game.global.level, this.addEnemyBoss, this);
        game.time.events.loop(1000-20*game.global.level, this.createEnemyBossBullet, this);

        this.life = 5;
        this.bombNo = Math.ceil(game.global.level/10);
        this.skillNo = Math.ceil(game.global.level/10);
        this.isInvisible = false;
        this.skillTimer = game.time.now;
        this.potionPosition = -100;
        this.scoreDisplay = game.add.text(5, 5, 'Score: ' + game.global.score, {font: '25px Courier', fill: '#ffffff'});
        this.scoreDisplay.anchor.setTo(0, 0);
        this.volumeDisplay = game.add.text(5, 30, 'Volume: ' + game.global.volume, {font: '25px Courier', fill: '#ffffff'});
        this.volumeDisplay.anchor.setTo(0, 0);
        this.lifeDisplay = game.add.text(game.width-5, 5, 'Life: ' + this.life, {font: '25px Courier', fill: '#ffffff'});
        this.lifeDisplay.anchor.setTo(1, 0);
        this.bombDisplay = game.add.text(game.width-5, 30, 'Bomb: ' + this.bombNo, {font: '25px Courier', fill: '#ffffff'});
        this.bombDisplay.anchor.setTo(1, 0);
        this.skillDisplay = game.add.text(game.width-5, 55, 'Invisible: ' + this.skillNo, {font: '25px Courier', fill: '#ffffff'});
        this.skillDisplay.anchor.setTo(1, 0);
    },
    update: function() {
        this.controlPlayer();
        game.physics.arcade.overlap(this.bulletGroup, this.enemyGroup, this.hitEnemy, null, this);
        if(this.isInvisible == false) game.physics.arcade.overlap(this.enemyBulletGroup, this.player, this.hitPlayer, null, this);
        if(this.helper) {
            if(Math.floor(this.helper.x) <= this.potionPosition + 3 && Math.floor(this.helper.x) >= this.potionPosition - 3) 
                this.createPotion();
            game.physics.arcade.overlap(this.bulletGroup, this.helper, this.hitHelper, null, this);
            game.physics.arcade.overlap(this.player, this.potion, this.healPlayer, null, this);
        }
        this.bgtile.tilePosition.y += 1 + 0.2*game.global.level;
        this.controlVolume();
    },
    controlPause: function() {
        if(game.paused == false) {
            this.pauseText = game.add.text(game.width/2, game.height/2, 'Press P to resume', {font: '30px "Comic Sans MS"', fill: '#ffffff'});
            this.pauseText.anchor.setTo(0.5, 0.5);
            game.paused = true;
        }    
        else {
            this.pauseText.kill();
            game.paused = false;
        }
    },
    addEnemy: function() {
        var enemy = this.enemyGroup.getFirstDead();
        if(!enemy) return;
        var direction = game.rnd.pick([-1, 1]);
        if(direction == 1) enemy.reset(0, 50);
        else enemy.reset(game.width, 50);
        enemy.body.velocity.x = (100 + 10*game.global.level) * direction;
        enemy.body.velocity.y = 30 + 2*game.global.level;
        enemy.body.bounce.x = 1;
    },
    addEnemyBoss: function() {
        if(!this.enemyBoss || this.enemyBoss.alive == false) {
            this.enemyBoss = game.add.sprite(0, 0, 'enemy');
            game.physics.arcade.enable(this.enemyBoss);
            if(!this.enemyBoss) return;
            var direction = game.rnd.pick([-1, 1]);
            if(direction == 1) this.enemyBoss.reset(0, 50);
            else this.enemyBoss.reset(game.width, 50);
            this.enemyBoss.body.velocity.x = (80 + 10*game.global.level) * direction;
            this.enemyBoss.body.velocity.y = 30 + 2*game.global.level;
            this.enemyBoss.body.bounce.x = 1;
            this.enemyBoss.scale.x = 1;
            this.enemyBoss.scale.y = 1;
            this.enemyBoss.anchor.x = 0.5;
            this.enemyBoss.anchor.y = 1;
        }
    },
    addHelper: function() {
        if(!this.helper || this.helper.alive == false) {
            this.helper = game.add.sprite(0, 0, 'helper');
            game.physics.arcade.enable(this.helper)
            this.helper.outOfBoundsKill = true;
            this.helper.checkWorldBounds = true;
            if(!this.helper) return;
            var direction = game.rnd.pick([-1, 1]);
            if(direction == 1) this.helper.reset(0, 50);
            else this.helper.reset(game.width, 50);
            this.helper.scale.setTo(0.4, 0.4);
            this.helper.body.velocity.x = (60 + 10*game.global.level) * direction;
            this.helper.body.velocity.y = 10 + 2*game.global.level;
            this.helper.body.bounce.x = 1;
            this.helper.anchor.x = 0.5;
            this.helper.anchor.y = 1;
            this.potionPosition = game.rnd.between(0, 800);
            console.log(this.potionPosition);
        }
    },
    createEnemyBullet: function() {
        this.enemyGroup.forEachAlive(function(enemy) {
            if(enemy.scale.x == 0.5) {
                this.enemyBullet = game.add.sprite(enemy.x, enemy.y, 'enemyBullet');
                gameState.enemyBulletGroup.add(this.enemyBullet);
                this.enemyBullet.scale.setTo(0.3, 0.3);
                this.enemyBullet.body.velocity.y = 800 + 10*game.global.level;
            }
        });
    },
    createEnemyBossBullet: function() {
        if(this.enemyBoss && this.enemyBoss.alive == true) {
            for(i=-1; this.enemyBoss && i<2; i++) {
                this.enemyBullet = game.add.sprite(this.enemyBoss.x, this.enemyBoss.y, 'enemyBullet');
                gameState.enemyBulletGroup.add(this.enemyBullet);
                this.enemyBullet.scale.setTo(0.3, 0.3);
                this.enemyBullet.body.velocity.y = 1000 + 10*game.global.level;
                if(i!=0) this.enemyBullet.body.velocity.x = i*(500 + 2*game.global.level);
            }
        }
    },
    createPotion: function() {
        console.log('createPotion');
        this.potion = game.add.sprite(this.potionPosition, this.helper.y, 'potion');
        game.physics.arcade.enable(this.potion);
        this.potion.scale.setTo(0.3, 0.3);
        this.potion.anchor.setTo(0.5, 1);
        this.potion.body.velocity.y = 200 + 10*game.global.level;
        this.potionPosition = -1000;
    },
    controlPlayer: function() {
        if(this.weaponKey.isDown && this.bombNo != 0) {
            if(this.bombAnimation.isPlaying == false) this.player.animations.play('holdingBomb');
            if(this.spaceKey.isDown && game.time.now > this.bulletTime) {
                this.fireBomb();
            }
        }
        if(this.skillKey.isDown && this.skillNo != 0) {
            this.player.alpha = 0.5;
            this.skillTimer = game.time.now + 5000;
            this.isInvisible = true;
            this.skillNo--;
            this.skillDisplay.text = 'Invisible: ' + this.skillNo;
        }
        if(this.isInvisible && game.time.now > this.skillTimer) {
            this.player.alpha = 1;
            this.skillTimer = game.time.now;
            this.isInvisible = false;
        }
        else {
            this.bombAnimation.stop();
            if(this.spaceKey.isDown && game.time.now > this.bulletTime) {
                this.fireBullet();
            }
        }
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -300 - 15*game.global.level;
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 300 + 15*game.global.level;
        }
        else {
            this.player.body.velocity.x = 0;
        }
    },
    fireBullet: function() {
        //console.log('fire');
        this.player.animations.play('fireGun');
        game.add.audio('effect1', game.global.volume-0.1, false).play();
        this.bulletTime = game.time.now + 300 - 2*game.global.level;
        this.bullet = game.add.sprite(this.player.x+45, this.player.y-10, 'bullet', this.bulletGroup);
        this.bulletGroup.add(this.bullet);
        this.bullet.scale.setTo(0.1, 0.1);
        this.bullet.body.velocity.y = -350 + 15*game.global.level;
        game.add.tween(this.bullet.scale).to({x: 0.4, y: 0.4}, 800).start();
        game.add.tween(this.bullet).to({alpha: 0.6}, 100).start();
    },
    fireBomb: function() {
        this.bombAnimation.stop();
        this.bulletTime = game.time.now + 300 - 10*game.global.level;
        this.bombNo--;
        this.bombDisplay.text = 'Bomb: ' +this.bombNo;
        this.bomb = game.add.sprite(this.player.x+45, this.player.y-10, 'bomb', this.bulletGroup);
        this.bomb.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.bomb);
        this.bomb.scale.setTo(0.1, 0.1);
        game.add.tween(this.bomb).to({x: game.width/2, y: game.height/2}, 500).start();
        game.add.tween(this.bomb.scale).to({x: 0.7, y: 0.7}, 500).start();
        game.time.events.add(500, this.bombExplode, this);
    },
    bombExplode: function() {
        this.bombEmitter.start(true, 1000, null, 15);
        game.add.audio('effect2', game.global.volume + 0.1, false).play();
        this.bomb.kill();
        if(this.enemyBoss && this.enemyBoss.alive == true) {
            this.enemyBoss.kill();
            gameState.updateScore(3);
        }
        this.enemyGroup.forEachAlive(function(enemy) {
            enemy.kill();
            gameState.updateScore(1);
        });
    },
    hitEnemy: function(enemy, bullet) {
        this.bulletEmitter.x = bullet.x;
        this.bulletEmitter.y = bullet.y;
        this.bulletEmitter.start(true, 800, null, 15);
        game.add.audio('effect3', game.global.volume, false).play();
        bullet.kill();
        enemy.kill();
        this.updateScore(1);
        //this.addEnemy();
    },
    hitPlayer: function(player, bullet) {
        bullet.kill();
        this.player.animations.play('injured');
        this.life--;
        this.lifeDisplay.text = 'Life: ' + this.life;
        console.log(this.life);
        if(this.life==0) this.gameOver();
    },
    healPlayer: function(player, bullet) {
        bullet.kill();
        this.player.animations.play('injured');
        this.life++;
        this.lifeDisplay.text = 'Life: ' + this.life;
        console.log('healed');
    },
    hitHelper: function(helper, bullet) {
        this.bulletEmitter.x = bullet.x;
        this.bulletEmitter.y = bullet.y;
        this.bulletEmitter.start(true, 800, null, 15);
        game.add.audio('effect3', game.global.volume, false).play();
        bullet.kill();
        helper.kill();
        this.updateScore(-3);
    },
    updateScore: function(score) {  
        game.global.score += score;
        this.scoreDisplay.text = 'Score: ' + game.global.score;
        if(game.global.score >= game.global.level * 10) this.levelUp();
    },
    levelUp: function() {
        this.player.kill();
        game.state.start('levelUp');
    },
    gameOver: function() {
        this.player.kill();
        game.state.start('gameOver');
        this.bgm.stop();
    },
    controlVolume: function() {
        if(this.cursor.up.isDown && game.global.volume<1) {
            game.global.volume += 0.01;
        }
        if(this.cursor.down.isDown && game.global.volume>0) {
            game.global.volume -= 0.01;
        }
        this.bgm.volume = game.global.volume;
        this.volumeDisplay.text = 'Volume: ' + Math.floor(game.global.volume*10)/10;
    }
};

var levelState = {
    create: function() {
        game.add.backgroundColor = '#000000';
        game.add.image(0, 0, 'background');
        game.global.level++;
        this.title = game.add.text(game.width/2, game.height/2, 'Level ' + game.global.level + '!!', {font: '60px "Comic Sans MS"', fill: '#ffffff'});
        this.title.anchor.setTo(0.5, 0.5);
        game.add.tween(this.title.scale).to({x: 1.5, y: 1.5}, 1500).start();
        this.emitter = game.add.emitter(game.width/2, game.height/2, 15);
        this.emitter.makeParticles('particle');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(0.3, 0, 0.3, 0, 3000);
        this.emitter.gravity = 200;
    },
    update: function() {
        this.emitter.start(true, 3000, null, 15);
        game.time.events.add(3000, this.nextState, this);
    },
    nextState: function() {
        game.state.start('game');
    }
}

var gameOverState = {
    create: function() {
        game.add.backgroundColor = '#000000';
        game.add.image(0, 0, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        this.enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.backSpaceKey = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
        game.add.text(game.width/2, game.height/4, 'Game Over', {font: '60px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 250, 'Score: ' + game.global.score, {font: '40px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 450, 'Enter => Play again', {font: '20px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 500, 'Backspace => Back to Menu', {font: '20px "Comic Sans MS"', fill: '#ffffff'}).anchor.setTo(0.5, 0.5);

        this.bgm = game.add.audio('bgm1', game.global.volume, true);
        this.bgm.play();
    },
    update: function() {
        if(this.enterKey.isDown) {
            game.state.start('game');
            this.bgm.stop();
        }
        if(this.backSpaceKey.isDown) {
            game.state.start('menu');
            this.bgm.stop();
        }
        game.global.score = 0;
        game.global.level = 1;
        this.controlVolume();
    },
    controlVolume: function() {
        if(this.cursor.up.isDown && game.global.volume<1) {
            game.global.volume += 0.1;
        }
        if(this.cursor.down.isDown && game.global.volume>0) {
            game.global.volume -= 0.1;
        }
        this.bgm.volume = game.global.volume;
    }
}

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
game.global = {
    score: 0,
    level: 1,
    volume: 0.7
};
game.state.add('preload', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);
game.state.add('levelUp', levelState);
game.state.add('gameOver', gameOverState);
game.state.start('preload');



